<?php
/**
 * Clean up the_excerpt()
 */
function roots_excerpt_more() {
  return ' &hellip; <a href="' . get_permalink() . '">' . __('Continued', 'roots') . '</a>';
}
add_filter('excerpt_more', 'roots_excerpt_more');

/*==============================================================

Carawebs Phone Class

==============================================================*/
class carawebs_phone_widget extends WP_Widget {


  /** constructor -- name this the same as the class above */
  function carawebs_phone_widget() {
    parent::WP_Widget(false, $name = 'Carawebs Phone Widget');
  }

  /** @see WP_Widget::widget -- do not rename this */
  function widget($args, $instance) {
    extract( $args );
    $title 		= apply_filters('widget_title', $instance['title']);
    $introtext 	= $instance['introtext'];
    $telnumber   = $instance['telnumber'];

    ?>
    <?php echo $before_widget; ?>
    <?php if ($title) {
      echo $before_title, $title, $after_title;
    } ?>
    <?php if ($introtext) {
      ?><p><?php echo $introtext; ?><br><?php
    } ?>
    <div class="desktop-phone"><i class="glyphicon glyphicon-phone-alt"></i>&nbsp;&nbsp;<?php echo $telnumber; ?></div>
    <div class="mobile-phone">
      <a href="tel:<?php echo $telnumber; ?>" class="btn btn-default btn-primary">
        <span class="glyphicon glyphicon-phone-alt"></span> Click to Call</a>
      </div>
    </p>

    <?php echo $after_widget; ?>
    <?php
  }

  /** @see WP_Widget::update -- do not rename this */
  function update($new_instance, $old_instance) {
    $instance = $old_instance;
    $instance['title'] = strip_tags($new_instance['title']);
    $instance['introtext'] = strip_tags($new_instance['introtext']);
    $instance['telnumber'] = strip_tags($new_instance['telnumber']);
    return $instance;
  }

  /** @see WP_Widget::form -- do not rename this */
  function form($instance) {

    $title 		= esc_attr($instance['title']);
    $introtext	= esc_attr($instance['introtext']);
    $telnumber	= esc_attr($instance['telnumber']);
    ?>
    <p>Enter your Phone contact details here. If you leave a field blank, it will not display on the page.</p>
    <p>
      <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label>
      <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
    </p>
    <p>
      <label for="<?php echo $this->get_field_id('introtext'); ?>"><?php _e('Paste the text you\'d like to accompany the phone number'); ?></label>
      <input class="widefat" id="<?php echo $this->get_field_id('introtext'); ?>" name="<?php echo $this->get_field_name('introtext'); ?>" type="text" value="<?php echo $introtext; ?>" />
    </p>
    <p>
      <label for="<?php echo $this->get_field_id('telnumber'); ?>"><?php _e('Enter the phone number'); ?></label>
      <input class="widefat" id="<?php echo $this->get_field_id('telnumber'); ?>" name="<?php echo $this->get_field_name('telnumber'); ?>" type="text" value="<?php echo $telnumber; ?>" />
    </p>
    <?php
  }


} // end class example_widget
add_action('widgets_init', create_function('', 'return register_widget("carawebs_phone_widget");'));

/*==============================================================

Carawebs Social Widget Class

==============================================================*/
class carawebs_social_widget extends WP_Widget {


  /** constructor -- name this the same as the class above */
  function carawebs_social_widget() {
    parent::WP_Widget(false, $name = 'Carawebs Social Widget');
  }

  /** @see WP_Widget::widget -- do not rename this */
  function widget($args, $instance) {
    extract( $args );
    $title 		= apply_filters('widget_title', $instance['title']);
    $twitter 	= $instance['twitter'];
    $facebook   = $instance['facebook'];
    $email      = $instance['email'];
    ?>
    <?php echo $before_widget; ?>
    <?php if ($title) {
      echo $before_title, $title, $after_title;
    } ?>

    <ul class="i-ul">
      <?php if (!empty($twitter)): ?>
        <li><a href="<?php echo $twitter; ?>"><i class="icon-twitter i-li"></i>&nbsp;Follow us on Twitter</a></li>
      <?php endif;
      if (!empty($facebook)): ?>
      <li><a href="<?php echo $facebook; ?>"><i class="icon-facebook i-li"></i>&nbsp;Like us on Facebook</a></li>
    <?php endif;
    if (!empty($email)): ?>
    <li><a href="mailto:<?php echo antispambot($email, 1); ?>"><i class="icon-mail-1 i-li"></i>&nbsp;Send us an email</a></li>
  <?php endif; ?>
</ul>
<?php echo $after_widget; ?>
<?php
}

/** @see WP_Widget::update -- do not rename this */
function update($new_instance, $old_instance) {
  $instance = $old_instance;
  $instance['title'] = strip_tags($new_instance['title']);
  $instance['twitter'] = strip_tags($new_instance['twitter']);
  $instance['facebook'] = strip_tags($new_instance['facebook']);
  $instance['email'] = strip_tags($new_instance['email']);
  return $instance;
}

/** @see WP_Widget::form -- do not rename this */
function form($instance) {

  $title 		= esc_attr($instance['title']);
  $twitter	= esc_attr($instance['twitter']);
  $facebook	= esc_attr($instance['facebook']);
  $email	= esc_attr($instance['email']);
  ?>
  <p>Enter your contact details here. If you leave a field blank, it will not display on the page.</p>
  <p>
    <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label>
    <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
  </p>
  <p>
    <label for="<?php echo $this->get_field_id('twitter'); ?>"><?php _e('Paste the URL of your Twitter'); ?></label>
    <input class="widefat" id="<?php echo $this->get_field_id('twitter'); ?>" name="<?php echo $this->get_field_name('twitter'); ?>" type="text" value="<?php echo $twitter; ?>" />
  </p>
  <p>
    <label for="<?php echo $this->get_field_id('facebook'); ?>"><?php _e('Enter Facebook URL'); ?></label>
    <input class="widefat" id="<?php echo $this->get_field_id('facebook'); ?>" name="<?php echo $this->get_field_name('facebook'); ?>" type="text" value="<?php echo $facebook; ?>" />
  </p>
  <p>
    <label for="<?php echo $this->get_field_id('email'); ?>"><?php _e('Enter your email address'); ?></label>
    <input class="widefat" id="<?php echo $this->get_field_id('email'); ?>" name="<?php echo $this->get_field_name('email'); ?>" type="text" value="<?php echo $email; ?>" />
  </p>
  <?php
}


} // end class example_widget
add_action('widgets_init', create_function('', 'return register_widget("carawebs_social_widget");'));
