<!-- Main Content Row -->
<div class="row content">

    <div class="col-md-8">
      <img class="img-responsive" src="<?php the_field('main_image'); ?>" alt="">
    </div>

    <div class="col-md-4">
      <?php the_content(); ?>
    </div>

</div>
<!-- /.row -->

<!-- Services Row -->
<div class="row">

    <div class="col-lg-12">
      <h2 class="page-header">My Services</h2>
    </div>

    <div class="col-md-4 col-xs-6">
      <a href="#">
        <img class="img-responsive portfolio-item" src="<?php the_field('column_1_image'); ?>" alt="">
      </a>
        <?php the_field('column_1_content'); ?>
    </div>

    <div class="col-md-4 col-xs-6">
      <a href="#">
        <img class="img-responsive portfolio-item" src="<?php the_field('column_2_image'); ?>" alt="">
      </a>
      <?php the_field('column_2_content'); ?>
    </div>

    <div class="col-md-4 col-xs-6">
      <a href="#">
        <img class="img-responsive portfolio-item" src="<?php the_field('column_3_image'); ?>" alt="">
      </a>
      <?php the_field('column_3_content'); ?>
    </div>

</div>
<!-- /.row -->

<?php wp_link_pages(array('before' => '<nav class="pagination">', 'after' => '</nav>')); ?>
