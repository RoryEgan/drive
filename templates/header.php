<header id="header" class="banner" role="contentinfo">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-6">
        <?php dynamic_sidebar('sidebar-header'); ?>
      </div>
      <div class="col-md-6">
        <div class="pull-right">
          <?php dynamic_sidebar('sidebar-header-right'); ?>
        </div>
      </div>
    </div>
  </div>
</header>
